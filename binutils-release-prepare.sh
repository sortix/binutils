#!/bin/sh -e
./binutils-reconf.sh
./configure --disable-werror
${MAKE-make} -j8
${MAKE-make} distclean
