#!/bin/sh -e
export PATH="$HOME/opt/autoconf-2.64/bin:$PATH"
export PATH="$HOME/opt/automake-1.11.6/bin:$PATH"
echo autogen Makefile.def
autogen Makefile.def
BINUTILS_M4_DIR=$PWD/config
(find ./ -name configure.ac && find ./ -name configure.in) | \
while read filepath; do
  echo "(cd $(dirname "$filepath") && autoreconf -I "$BINUTILS_M4_DIR")"
  (cd "$(dirname "$filepath")"/ &&
    (autoreconf -I "$BINUTILS_M4_DIR" ||
     (true) &&
     (echo "Got an error, trying again:";
      autoreconf -I "$BINUTILS_M4_DIR")))
done
find ./ -type d -name autom4te.cache | while read filepath; do
  rm -rf "$filepath"
done
echo "$0: done"
